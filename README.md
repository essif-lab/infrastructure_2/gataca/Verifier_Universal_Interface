# VERIFIER UNIVERSAL INTERFACE (VUI)

Interop efforts and results moved to the [interoperability repository](https://gitlab.grnet.gr/essif-lab/interoperability/verifierapis)

Golang verification library moved to [github](https://github.com/gataca-io/vui-core)



